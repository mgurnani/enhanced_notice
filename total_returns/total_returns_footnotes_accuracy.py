import pandas as pd
import glob
import argparse
import warnings
warnings.filterwarnings("ignore")
import total_returns_footnotes


def check_values(expected, actual):
  expected = float(expected)
  if actual[len(actual) - 1] == '%' and actual[len(actual) - 2] == ')':
    actual = float(actual[1: len(actual) - 2])
  elif actual[len(actual) - 1] == '%':
    actual = float(actual[0: len(actual) - 1])
  elif actual[len(actual) - 1] == ')':
    actual = float(actual[1: len(actual) - 1])
  else:
    actual = float(actual)
  if expected == actual or abs(int(actual * 10e4))== abs(int(expected * 10e6)):
    return True
  return False

def is_float(num):
  try:
    float(num)
  except:
    return False
  return True

def accuracy(expected, actual, df, file):
  count = 0.0; correct = 0.0;
  for i in range(len(expected)):
      if  str(expected[i]).lower().strip() == str(actual[i]).lower().strip():
        correct += 1
      else:
        index = 0
        if df.empty == False:
          index = max(df.index) + 1
        df.loc[index] = [expected[i], actual[i], expected, actual, file]
        #print(expected[i][j], ' vs ', actual[i][j])
      count += 1
  return correct/count * 100, correct, count


def compute_accuracy_file(file, df):
  accur = 0; correct = 0; count = 0
  gold_file = glob.glob(file + '/*.xlsx')
  text_file = glob.glob(file + '/*AR.htm*')
  try:
    excel = load_gold_standard(gold_file[0])
    gold_standard = extract_gold_standard(excel)
    localized_string = total_returns_footnotes.localize(text_file[0])
    if localized_string:
      extracted_points = total_returns_footnotes.extract(localized_string)
      accur, correct, count = accuracy(gold_standard, extracted_points, df, file)
  except Exception as e:
    print(str(e))
    print(file)
  return accur, correct, count

def extract_gold_standard(excelObj):
  # Extracting Total Returns
  footnotes = []
  start = False; end = False
  for index, row in excelObj.iterrows():
    list = []
    for items in row:
      if not pd.isna(items) and str(items).lower() not in 'Page'.lower():
        if items == 'Foot Notes':
          start = True
        elif items == 'Top Holdings':
          end = True
        elif start and not end:
          list.append(items)
    if len(list) != 0:
      footnotes.append(list)
  return footnotes

def load_gold_standard(gold_standard_file):
  excel = pd.read_excel(gold_standard_file)
  return excel

def compute_accuracy_dir(dir, df):
  correct = 0.0; count = 0.0;
  list = glob.glob(dir +'/*')
  for file in list:
    local_accuracy, local_correct, local_count = compute_accuracy_file(file, df)
    correct += local_correct
    count += local_count
  return correct/count * 100

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')
  group.add_argument('-d', '--directory', type=str, help='Path to directory')

  args = parser.parse_args()
  file = args.file
  dir = args.directory

  logs_data = {'Expected': [], 'Actual': [], 'ExpectedData': [], 'ActualData': [],
              'File': []}
  # Convert the dictionary into DataFrame
  df = pd.DataFrame(logs_data)

  if file:
      accuracy, correct, count = compute_accuracy_file(file, df)
      print('File accuracy:', accuracy)

  if dir:
      print('Final Accuracy:', compute_accuracy_dir(dir, df))

  df.to_csv('failed_analysis.csv')