import glob
import argparse
from bs4 import BeautifulSoup as bs
import re
import warnings
import total_returns_table

warnings.filterwarnings("ignore")

def extract(localized_string):
  soup = bs(localized_string, 'lxml')
  data = soup.find_all('a')
  if not data:
    data = soup.find_all('p')
  result_body = []
  for result in data:
    ans = standardize(result.text)
    if ans:
      result_body.append(ans)
  return result_body


def standardize(data):
  if isinstance(data, str):
    data = data.replace('\n', ' ').replace('\xa0', '').replace('\\n',' ').strip()
    data = re.sub('\s+', ' ', data)
    data = re.sub("\[.*\]", '', data)
    #data = data.replace('"','')
  return data.strip()

def fetch_title_ten_thousand(text):
  match = re.search("\$10,000 Over 10 Years", text)
  if match:
    return match.start()
  return None

def get_benchmark_offset_start(text):
  x = re.search("The\s?(\\n)?chart\s?(\\n)?shows\s?(\\n)?how\s?(\\n)?the\s?(\\n)?value\s?(\\n)?of\s?(\\n)?your\s?(\\n)?investment\s?(\\n)?would\s?(\\n)?have\s?(\\n)?changed,\s?(\\n)?and\s?(\\n)?also\s?(\\n)?shows\s?(\\n)?how\s?(\\n)?the", text)
  if x:
    return x.end()
  return None

def get_benchmark_offset_end(text):
  x = re.search("performed over the same period", text)
  if x:
    return x.start()
  return None

def localize(filename):
  table, start_offset, end_offset_table = total_returns_table.localize(filename, None)
  if not end_offset_table:
    return None, None, None
  f = open(filename, "r", encoding="utf8", errors='ignore')
  text = f.read()#.replace('\n', '\\n')
  f.close()
  offset_benchmark_title = fetch_title_ten_thousand(text[end_offset_table:])
  if not offset_benchmark_title:
    return None, None, None
  benchmark_start_offset = get_benchmark_offset_start(text[end_offset_table+offset_benchmark_title: ])
  benchmark_end_offset = get_benchmark_offset_end(text[end_offset_table+offset_benchmark_title: ])
  if not benchmark_start_offset or not benchmark_end_offset:
    return None, None, None
  final_start_offset = end_offset_table+offset_benchmark_title+ benchmark_start_offset
  final_end_offset = end_offset_table + offset_benchmark_title+ benchmark_end_offset
  return text[ final_start_offset: final_end_offset ], final_start_offset, final_end_offset


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')

  args = parser.parse_args()
  file = args.file
  text_file = glob.glob(file + '/*_AR.htm*')
  if not text_file:
    text_file = glob.glob(file + '/* AR.htm*')
  localized_string, start_offset, end_offset = localize(text_file[0])
  if localized_string:
    extracted_points = extract(localized_string)
    print(extracted_points)
    print('offsets are {}, {}'.format(start_offset, end_offset))