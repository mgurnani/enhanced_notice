import glob
import argparse
from bs4 import BeautifulSoup as bs
import re
import warnings
import total_returns_table

warnings.filterwarnings("ignore")

def extract(localized_string):
  soup = bs(localized_string, 'lxml')
  data = soup.find_all('p')
  result_body = []
  for result in data:
    ans = standardize(result.text)
    if ans:
      result_body.append(ans)
  print(result_body)
  return result_body


def standardize(data):
  if isinstance(data, str):
    data = data.replace('\n', ' ').replace('\xa0', '').replace('\\n',' ').strip()
    data = re.sub('\s+', ' ', data)
    data = re.sub("\[.*\]", '', data)
    #data = data.replace('"','')
  return data.strip()

def fetch_offset_bench_mark(text):
  return re.search("\$10,000 Over 10 Years", text).start()

def localize(filename):
  table, start_offset, end_offset = total_returns_table.localize(filename, None)
  if not end_offset:
    return None
  f = open(filename, "r")
  text = f.read()#.replace('\n', '\\n')
  f.close()
  offset_benchmark = fetch_offset_bench_mark(text[end_offset:])
  if not offset_benchmark:
    return None
  return text[end_offset : end_offset + offset_benchmark], end_offset, end_offset + offset_benchmark


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')

  args = parser.parse_args()
  file = args.file
  text_file = glob.glob(file + '/*AR.htm*')
  localized_string, start_offset, end_offset = localize(text_file[0])
  if localized_string:
    extracted_points = extract(localized_string)
    print('Offsets are {}, {}'.format(start_offset, end_offset))