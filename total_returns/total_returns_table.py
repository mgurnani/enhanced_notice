import glob, os
import argparse
from bs4 import BeautifulSoup as bs
import re
import warnings
warnings.filterwarnings("ignore")

def extract(localized_string, filename, fundname):
  if fund_contains_class(filename, fundname):
    result = extract_record(localized_string, filename, fundname)
  else:
    result = extract_table(localized_string)
  return result

def fund_contains_class(filename, fundname):
  if fundname and re.search('\s+([a-zA-Z])$', fundname):
    return True
  elif fundname:
    return False
  elif re.search('(?i)Class', filename, re.IGNORECASE):  ###Delete this part later
    return True
  return False

def extract_record(localized_string, filename, fundname):
  if fundname:
    fund_class = re.search('\s+([a-zA-Z])$', fundname)
    reg = '(?i)Class ' + fund_class.group(1).strip()
  else:   ##Delete else part later
    fund_class = re.search("((?i)Class)\s+(\w)", filename)
    reg = '(?i)'+fund_class.group(1).strip() + ' ' + fund_class.group(2).strip()
  table_soup = bs(localized_string, 'lxml')
  table_rows = table_soup.find_all('tr')
  l = []
  for tr in table_rows:
    td = tr.find_all('td')
    row = [standardize(tr.text) for tr in td]
    if len(l) == 1 and re.search(reg, str(row)):
      l.append(row)
    elif len(l) == 0:
      l.append(row)
  return l


def extract_table(localized_string):
  table_soup = bs(localized_string, 'lxml')
  table_rows = table_soup.find_all('tr')
  l = []
  for tr in table_rows:
    td = tr.find_all('td')
    row = [standardize(tr.text) for tr in td]
    l.append(row)
  #print(l)
  return l


def standardize(data):
  if isinstance(data, str):
    data = data.replace('\n', '').replace('\xa0', '').replace('\\n','').strip()
    data = re.sub('\s+', ' ', data)
    data = re.sub("\[.*\]", '', data)
    data = re.sub('\Â','', data)
  return data


def check_multiple_tables(regex, text):
  matches = re.finditer(regex, text)
  i = 0
  for match in matches:
    i += 1
  if i > 1:
    return True
  return False

def extract_relevant_title(filename, fundname, text):
  if fundname:
    title = fundname
  else:
    title = os.path.basename(filename)   ##Delete this part later
  fund_name = re.search("([^\d.]*)(\d+)", title)
  fund_list = fund_name.group(1).strip().split(' ')
  fund_name_reg = '\s'.join(fund_list)
  #Fidelity\s*
  #fund_name_reg = 'Fidelity\s*Asset\s*Manager'
  reg = '(?i)'+ fund_name_reg + '[^A-Za-z0-9]*' + fund_name.group(2)
  matches = re.finditer(reg, text, re.MULTILINE)
  i = 0
  for match in matches:
    i += 1
    if i == 2:
      return match
  return None

def localize(filename, fund_name):
  regex_title = "Average\s*Annual\s*Total\s*Returns"
  regex_table = "(<table[^>]*>(?:.|\n)*?<\/table>)"
  f = open(filename, "r")
  text = f.read()#.replace('\n', '\\n')
  f.close()
  title = None
  if check_multiple_tables(regex_title, text):
    title = extract_relevant_title(filename, fund_name, text)
  if not title:
    title = re.search(regex_title, text, re.IGNORECASE)
  if not title:
    return None, None, None

  title_offset = title.end()
  table_search = re.search(regex_table, text[title_offset: ])
  table_html = text[table_search.start() + title_offset :table_search.end() + title_offset]
  return table_html, title_offset + table_search.start(), title_offset + table_search.end()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')
  parser.add_argument('--fund_name', type=str, help='Fund Name')

  args = parser.parse_args()
  file = args.file
  fund_name = args.fund_name
  text_file = glob.glob(file + '/*_AR.htm*')
  if not text_file:
    text_file = glob.glob(file + '/* AR.htm*')
  localized_string, start_offset, end_offset = localize(text_file[0], fund_name)
  if localized_string:
    extracted_points = extract(localized_string, text_file[0], fund_name)
    print(extracted_points)
    print('Offsets are {}, {}'.format(start_offset,  end_offset))