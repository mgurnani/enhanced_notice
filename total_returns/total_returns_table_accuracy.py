import pandas as pd
import glob
import math, re
import argparse
import warnings
warnings.filterwarnings("ignore")
import total_returns_table

def check_values(expected, actual):
  expected = float(expected)
  if actual[len(actual) - 1] == '%' and actual[len(actual) - 2] == ')':
    actual = float(actual[1: len(actual) - 2])
  elif actual[len(actual) - 1] == '%':
    actual = float(actual[0: len(actual) - 1])
  elif actual[len(actual) - 1] == ')':
    actual = float(actual[1: len(actual) - 1])
  else:
    actual = float(actual)
  if expected == actual or math.isclose(abs(actual * 10e6), abs(expected * 10e8)):
    return True
  return False

def is_float(num):
  try:
    float(num)
  except:
    return False
  return True

def compare_words(w1, w2):
  w1 = re.sub('[^\w+]',' ', str(w1).lower().strip())
  w2 = re.sub('[^\w+]', ' ', str(w2).lower().strip())
  w1 = re.sub('\s+', ' ', w1)
  w2 = re.sub('\s+', ' ', w2)
  return w1 == w2

def accuracy(expected, actual, df, file):
  count = 0.0; correct = 0.0;
  for i in range(len(expected)):
    for j in range(len(expected[i])):
      if is_float(expected[i][j]) and check_values(expected[i][j], actual[i][j]):
        correct += 1
      elif compare_words(expected[i][j], actual[i][j]):
        correct += 1
      else:
        index = 0
        if df.empty == False:
          index = max(df.index) + 1
        df.loc[index] = [expected[i][j], actual[i][j], expected, actual, file]
        #print(expected[i][j], ' vs ', actual[i][j])
      count += 1
  return correct/count * 100, correct, count


def compute_accuracy_file(file, df):
  accur = 0; correct = 0; count = 0
  gold_file = glob.glob(file + '/*.xlsx')
  text_file = glob.glob(file + '/*_AR.htm*')
  if not text_file:
    text_file = glob.glob(file + '/* AR.htm*')
  if not text_file:
    print('Not found')
  try:
    excel = load_gold_standard(gold_file[0])
    gold_fund_name = excel.columns[1]
    gold_standard = extract_gold_standard(excel)
    localized_string, start_offset, end_offset = total_returns_table.localize(text_file[0], None)
    if localized_string:
      extracted_points = total_returns_table.extract(localized_string, text_file[0], None)
      accur, correct, count = accuracy(gold_standard, extracted_points, df, file)
    elif len(gold_standard) > 0:
      print(text_file[0], gold_standard)
  except Exception as e:
    print(str(e))
    print(file)
  return accur, correct, count

def extract_gold_standard(excelObj):
  # Extracting Total Returns
  total_returns = []
  start = False; end = False
  for index, row in excelObj.iterrows():
    list = []
    for items in row:
      if not pd.isna(items) and 'Page'.lower() not in str(items).lower():
        if items == 'Average Annual Total Returns':
          start = True
        elif items == 'Benchmark':
          end = True
        elif start and not end:
          list.append(total_returns_table.standardize(items))
    if len(list) != 0:
      total_returns.append(list)
  return total_returns


def standardize(data):
  if isinstance(data, str):
    data = data.replace('\n', '').replace('\xa0', '').replace('\\n','').strip()
    data = re.sub('\s+', ' ', data)
    data = re.sub("\[.*\]", '', data)
    data = re.sub('\Â','', data)
  return data

def load_gold_standard(gold_standard_file):
  excel = pd.read_excel(gold_standard_file)
  return excel

def compute_accuracy_dir(dir, df):
  correct = 0.0; count = 0.0;
  list = glob.glob(dir +'/*')
  for file in list:
    local_accuracy, local_correct, local_count = compute_accuracy_file(file, df)
    correct += local_correct
    count += local_count
  return correct/count * 100

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='data-point-extraction')
  group = parser.add_mutually_exclusive_group(required=True)
  group.add_argument('-f', '--file', type=str, help='Path to Full address')
  group.add_argument('-d', '--directory', type=str, help='Path to directory')

  args = parser.parse_args()
  file = args.file
  dir = args.directory

  logs_data = {'Expected': [], 'Actual': [], 'ExpectedData': [], 'ActualData': [],
              'File': []}
  # Convert the dictionary into DataFrame
  df = pd.DataFrame(logs_data)

  if file:
      accuracy, correct, count = compute_accuracy_file(file, df)
      print('File accuracy:', accuracy)

  if dir:
      print('Final Accuracy:', compute_accuracy_dir(dir, df))

  df.to_csv('failed_analysis.csv')